FROM ubuntu:latest

COPY ./run-tests.sh .

ENTRYPOINT ["bash", "./run-tests.sh"]

