## NOTE:
**This is solved!** I'm still happy to have figured out this issue and made this
little Proof of Concept repo, so I'm leaving it up. It was identified in
https://github.com/docker/compose/issues/9509 and fixed in Docker Compose V2 in
v2.7.0, specifically in the MR 
https://github.com/compose-spec/compose-go/pull/283 - fantasic!
This fix was already released at the point this PoC was created, but it was
less than a month beforehand and I was still running Docker Compose V2 v2.6.0

```
"Something something race conditions" - Michael Ball, August 2022.
```

# Docker Compose .env comments issue
This is a minimal reproduction of a difference between `docker-compose` and
`docker compose` that has been a mild pain.

Essentially, `docker-compose` (referred to from now on as "dash") pulls through
values from the .env file as anticipated, with no changes or quotation mark
required. `docker compose` (referred to as "space") chooses to interpret `#` as
the start of a comment, even if its in the middle of a password.
I can see why the "space" team might have chosen this, but it's highly
frustating if you're trying to migrate from one to another and it's more
annoying to change `your#super"secure'pass\word` to exclude certain characters
when you've got to deal with different parts of your organisation to do so.

I've included some extra examples of things that have been suggested to work
work around this problem, such putting the .env value in single and double
quotes, and putting the docker-compose.yaml value in single and double quotes.
Both of these don't work amazingly - either because things other than compose
may also need to read the value out of .env and shouldn't have to deal with
de-quoting them (along with the associated escaping of quotes in those values),
or because quoting it in the docker-compose.yaml file still doesn't save you
from "space" mishandling it.

### Potential resolutions:
1) Change the password. (Annoying, seems like a bad solution)
2) Keep using "dash", and avoid upgrading to "space". (Will fall behind)
3) Wait for "space" to implement a change to take care of this. (No guarantee)

### Running instructions:
1. Download and install docker, as per standard instructions.
1. Install `docker compose`, as per recommended method for your OS.
1. Install `docker-compose`, as per recommended method for your OS.
1. Run `docker-compose build -t docker-compose-env-comments-issue` and then
  `docker-compose run --rm docker-compose-env-comments-issue`. Tests should pass.
1. Repeat previous step, but start your commands with `docker compose` instead 
of `docker-compose`. Tests should fail.
