#!/bin/bash

set -e

test_vars=($test0 $test1 $test2 $test3 $test4 $test5)
expected_values=(
  "works_for_both" 
  "works_for_docker-compose#but_not_docker_compose"
  "single_quoted_in#.env"
  "double_quoted_in#.env"
  "single_quoted_in#docker-compose.yaml"
  "double_quoted_in#docker-compose.yaml"
)

i="0"
len=${#test_vars[@]}
while [ $i -lt $len ];
do
  if [[ ${test_vars[$i]} != ${expected_values[$i]} ]]
  then
    echo "Test $i failed. Expected << ${expected_values[$i]} >>, got << ${test_vars[$i]} >>."
  else
    echo "Test $i succeeded."
  fi
  i=$[$i+1]
done
